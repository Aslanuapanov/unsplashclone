import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'MainPage',
    component: () => import('@/views/MainPage')
  },
  {
    path: '/InnerPage/:id',
    name: 'InnerPage',
    props: true,
    component: () => import('@/views/InnerPage')
  },
  {
    path: '/FavoritePage',
    name: 'FavoritePage',
    component: () => import('@/views/FavoritePage')
  }
]

const router = new VueRouter({
  routes
})

export default router
